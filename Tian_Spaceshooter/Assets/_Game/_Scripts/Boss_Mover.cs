﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Mover : MonoBehaviour {
    private Rigidbody rb;
    public float speed;
	// Use this for initialization
	void Awake () {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.z < 13)
        {
            rb.velocity = new Vector3(rb.velocity.x,rb.velocity.y,0f);
            print("Stop");
        }
	}

}
