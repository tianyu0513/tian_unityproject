﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {
    private Rigidbody rb;
    private AudioSource audioSource;
    public float speed;
    public Boundary boundary;
    public float tilt;
    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    private float nextFire;

    public Slider healthBar;
    int maxHealth = 10;
    int health;

    public GameObject playerExplosion;
    private GameController gameController;
    


	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();

        health = maxHealth;
        UpdateHealthBar();

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        UpdateHealthBar();

        if (health <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        Instantiate(playerExplosion, transform.position, transform.rotation);
        gameController.GameOver();
        Destroy(gameObject);
    }

    void UpdateHealthBar()
    {
        healthBar.value = health / (float)(maxHealth);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            //GameObject clone = 
            MakeBullet();
            Invoke("MakeBullet", 0.10f);
            Invoke("MakeBullet", 0.20f);
        

        }
	}
    void MakeBullet() {
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);//as GameObject;
        audioSource.Play();
    }
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;
        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
            );

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);

    }
}
