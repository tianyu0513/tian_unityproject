﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDestroy : MonoBehaviour {
    private GameController gameController;
    public GameObject explosion;
    public GameObject smallExplosion;
    //public Slider healthBar;
    int maxHealth =30 ;
    int health;
    // Use this for initialization
    void Start () {
        health = maxHealth;
        //UpdateHealthBar();
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerBolt")
        {
            TakeDamage(1);
            Destroy(other.gameObject);
            Instantiate(smallExplosion, other.transform.position, transform.rotation);
        }
    }
    public void TakeDamage(int damage)
    {
        health -= damage;
        //UpdateHealthBar();

        if (health <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        Instantiate(explosion, transform.position, transform.rotation);
        gameController.BossFinish = true;
        Destroy(gameObject);

    }

}
